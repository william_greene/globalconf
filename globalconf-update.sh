#!/bin/bash

#******************************************************************
#**
#** globalConf master updater script
#**
#******************************************************************
#** 
#** Copyright (C) 2012 Snowball Factory Inc.
#** 
#******************************************************************

# what's my name
myname="${0##*/}"

# usage hints
function usage {
	echo "usage: ${myname} [-h | --help] [--verbose] [--conffile CONFILE] [--schemafile SCHEMAFILE]"
}

# some defines
schemauri="http://schema.awe.sm/globalConf"
schemaroot="node()"

# process args
verbose=""
conffile="/usr/local/etc/globalconf/globalconf.xml"
schemafile="/usr/local/etc/globalconf/globalConf.xsd"

while [ "$1" != "" ]; do
    case $1 in
		-h | --help )		usage
							exit 0
 							;;
 		--verbose )			verbose="$1"
 							;;
		--conffile )		shift
 							if [ -z "$1" ]
 							then
 								echo "${myname}: error: Missing parameter to arg --conffile" 
 								usage
 								exit 1
 							else
								conffile=$1
	 						fi
							;;
		--schemafile )		shift
 							if [ -z "$1" ]
 							then
 								echo "${myname}: error: Missing parameter to arg --schemafile" 
 								usage
 								exit 1
 							else
								schemafile=$1
	 						fi
							;;
        * )					echo "error: unknown arg: $1"
							usage
							exit 1
	esac
    shift
done

# verify expected files
if [ -n "${verbose}" ]
then
	echo "${myname}: info: Locating configuration file ${conffile}"
fi
if [ ! -f "${conffile}" ]
then
	echo "${myname}: error: Failed to locate configuration file ${conffile}"
	exit 1
fi
if [ -n "${verbose}" ]
then
	echo "${myname}: info: Locating schema file ${schemafile}"
fi
if [ ! -f "${schemafile}" ]
then
	echo "${myname}: error: Failed to locate schema file ${schemafile}"
	exit 1
fi

# check binaries needed
if [ -n "${verbose}" ]
then
	echo "${myname}: info: Checking for required binaries"
fi
binaries="xmlstarlet mkdir rmdir find"
for binary in ${binaries}
do
	check=`which ${binary}`
	if [ $? -ne 0 ]
	then
		echo "${myname}: error: failed to locate command ${binary}"
		exit 1
	fi
done

# validate the config file
if [ -n "${verbose}" ]
then
	echo "${myname}: info: Validating configuration file"
fi
if ! xmlstarlet val -q -s "${schemafile}" "${conffile}"
then
		echo "${myname}: error: failed to validate configuration file ${conffile} against schema ${schemafile}"
		exit 1
fi

# extract some information from the config file

if [ -n "${verbose}" ]
then
	echo "${myname}: info: Extracting confDDirectory"
fi
confddirectory=`xmlstarlet sel -N tns="${schemauri}" -t -v "${schemaroot}/tns:globalConfConfig/tns:confDDirectory" "${conffile}" | xmlstarlet unesc`
if [ -z "${confddirectory}" ]
then
		echo "${myname}: error: Failed to extract confDDirectory"
		exit 1
fi
if [ -n "${verbose}" ]
then
	echo "${myname}: info: confDDirectory = ${confddirectory}"
fi

if [ -n "${verbose}" ]
then
	echo "${myname}: info: Extracting confSkelDirectory"
fi
confskeldirectory=`xmlstarlet sel -N tns="${schemauri}" -t -v "${schemaroot}/tns:globalConfConfig/tns:confSkelDirectory" "${conffile}" | xmlstarlet unesc`
if [ -z "${confskeldirectory}" ]
then
		echo "${myname}: error: Failed to extract confSkelDirectory"
		exit 1
fi
if [ -n "${verbose}" ]
then
	echo "${myname}: info: confSkelDirectory = ${confskeldirectory}"
fi

if [ -n "${verbose}" ]
then
	echo "${myname}: info: Extracting confCredsDirectory"
fi
confcredsdirectory=`xmlstarlet sel -N tns="${schemauri}" -t -v "${schemaroot}/tns:globalConfConfig/tns:confCredsDirectory" "${conffile}" | xmlstarlet unesc`
if [ -z "${confcredsdirectory}" ]
then
		echo "${myname}: error: Failed to extract confCredsDirectory"
		exit 1
fi
if [ -n "${verbose}" ]
then
	echo "${myname}: info: confCredsDirectory = ${confcredsdirectory}"
fi

if [ -n "${verbose}" ]
then
	echo "${myname}: info: Extracting lockDirectory"
fi
lockdirectory=`xmlstarlet sel -N tns="${schemauri}" -t -v "${schemaroot}/tns:globalConfConfig/tns:lockDirectory" "${conffile}" | xmlstarlet unesc`
if [ -z "${lockdirectory}" ]
then
		echo "${myname}: error: Failed to extract lockDirectory"
		exit 1
fi
if [ -n "${verbose}" ]
then
	echo "${myname}: info: lockDirectory = ${lockdirectory}"
fi

# check that directories exist
if [ -n "${verbose}" ]
then
	echo "${myname}: info: Verifying directories"
fi
if [ ! -d "${confddirectory}" ]
then
		echo "${myname}: error: Failed to locate confDDirectory"
		exit 1
fi
if [ ! -d "${confskeldirectory}" ]
then
		echo "${myname}: error: Failed to locate confSkelDirectory"
		exit 1
fi
if [ ! -d "${confcredsdirectory}" ]
then
		echo "${myname}: error: Failed to locate confCredsDirectory"
		exit 1
fi
if [ ! -d "${lockdirectory}" ]
then
		echo "${myname}: error: Failed to locate lockDirectory"
		exit 1
fi

# check for a lock
locallock="${lockdirectory}/${myname}"
if [ -n "${verbose}" ]
then
	echo "${myname}: info: Checking for lock ${locallock}"
fi
if [ -d "${locallock}" ]
then
		echo "${myname}: error: Locked.  Exiting."
		exit 1
fi

# grab lock
if [ -n "${verbose}" ]
then
	echo "${myname}: info: Grabbing lock ${locallock}"
fi
if ! mkdir "${locallock}" > /dev/null 2>/dev/null
then
		echo "${myname}: error: Failed to grab locked.  Exiting."
		exit 1
fi	

# run the conf.d snippets
delayedexit=""
if [ -n "${verbose}" ]
then
	echo "${myname}: info: Executing conf.d snippets"
fi
confdfiles=`find "${confddirectory}" -type f -executable`
for confdfile in ${confdfiles}
do
	if [ -n "${verbose}" ]
	then
		echo "${myname}: info: Executing conf.d snippet: ${confdfile}"
	fi

	if ! "${confdfile}" "${verbose}" --conffile "${conffile}" --schemafile "${schemafile}"
	then
		echo "${myname}: error: Snippet execution failed for ${confdfile}"
		delayedexit="true"
	fi
done
if [ -n "${delayedexit}" ]
then
	echo "${myname}: error: At least one snippet execution failed.  Exiting."
	exit 1
fi

# release lock
if [ -n "${verbose}" ]
then
	echo "${myname}: info: Releasing lock ${locallock}"
fi
if ! rmdir "${locallock}" > /dev/null 2>/dev/null
then
		echo "${myname}: error: Failed to release locked.  Exiting."
		exit 1
fi	

exit 0
