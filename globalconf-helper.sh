#******************************************************************
#**
#** globalConf snippet helper
#**
#******************************************************************
#** 
#** Copyright (C) 2012 Snowball Factory Inc.
#** 
#******************************************************************

# the following are expected to be set in the calling script

#
# defines the subdirectory of conf.skel to look in
# it also is used for some output messages
#
#servicename="${myname}"

#
# input source files found in conf.skel/servicename
#
#inputfilenames+=()
#inputfilenames+=( "${servicename}.conf" )

#
# filenames of final output files.  can be different than
# input file names.  must include path
#
#outputfilenames=()
#outputfilenames+=( "${inputfilename}" )

#
# if set, chmod the output file to these perms
#
#outputfileperms=""

#
# if set, chown the output file to this ownership
# this supports owner:group 
#
#outputfileownership=""

#
# array of strings to replace in the input file
# use the += method of adding to the array so that
# the indices line up across arrays
#
#substitutionstrings=()
#substitutionstrings+=( "CHANGEME" )

#
# array of xpath queries to pull values from the conf.xml file
# use the += method of adding to the array so that
# the indices line up across arrays
#
#targetelementnames+=()
#targetelementnames+=( "/node()/tns:serverName" )

#
# name of service to reset via init.d scripts.  if blank,
# no attempt to restart a service is made
#
#restartservices=()
#restartservices+=( "${myname}" )


# usage hints
function usage {
	echo "usage: ${myname} [-h | --help] [--verbose] [--conffile CONFILE] [--schemafile SCHEMAFILE]"
}

# escape a string for sed
function escapeStringForSed {
	local thestring=$1

	# escape it
	escapeStringForSedReturn=`echo -n "${thestring}" | sed -e 's/[\/&]/\\\\&/g'`
}

# some defines
schemauri="http://schema.awe.sm/globalConf"

# process args
verbose=""
conffile="/usr/local/etc/globalconf/globalconf.xml"
schemafile="/usr/local/etc/globalconf/globalConf.xsd"

while [ "$1" != "" ]; do
    case $1 in
		-h | --help )		usage
							exit 0
 							;;
 		--verbose )			verbose="$1"
 							;;
		--conffile )		shift
 							if [ -z "$1" ]
 							then
 								echo "${myname}: error: Missing parameter to arg --conffile" 
 								usage
 								exit 1
 							else
								conffile=$1
	 						fi
							;;
		--schemafile )		shift
 							if [ -z "$1" ]
 							then
 								echo "${myname}: error: Missing parameter to arg --schemafile" 
 								usage
 								exit 1
 							else
								schemafile=$1
	 						fi
							;;
        * )					echo "error: unknown arg: $1"
							usage
							exit 1
	esac
    shift
done

# verify expected files
if [ -n "${verbose}" ]
then
	echo "${myname}: info: Locating configuration file ${conffile}"
fi
if [ ! -f "${conffile}" ]
then
	echo "${myname}: error: Failed to locate configuration file ${conffile}"
	exit 1
fi
if [ -n "${verbose}" ]
then
	echo "${myname}: info: Locating schema file ${schemafile}"
fi
if [ ! -f "${schemafile}" ]
then
	echo "${myname}: error: Failed to locate schema file ${schemafile}"
	exit 1
fi

# setup helpers
if [ -n "${restartservice}" ]
then
	if [ -n "${verbose}" ]
	then
		echo "${myname}: info: Loading helpers"
	fi
	helpers="/lib/init/vars.sh /lib/lsb/init-functions"
	for helper in ${helpers}
	do
		if [ ! -f  "${helper}" ]
		then
			echo "error: failed to locate ${helper}"
			exit 1
		else
			. ${helper}
		fi
	done
fi

# check binaries needed
if [ -n "${verbose}" ]
then
	echo "${myname}: info: Checking for required binaries"
fi
binaries="xmlstarlet invoke-rc.d mktemp rm mv sed chmod"
for binary in ${binaries}
do
	check=`which ${binary}`
	if [ $? -ne 0 ]
	then
		echo "${myname}: error: failed to locate command ${binary}"
		exit 1
	fi
done

# extract some information from the config file
if [ -n "${verbose}" ]
then
	echo "${myname}: info: Extracting confSkelDirectory"
fi
confskeldirectory=`xmlstarlet sel -N tns="${schemauri}" -t -v "/node()/tns:globalConfConfig/tns:confSkelDirectory" "${conffile}" | xmlstarlet unesc`
if [ -z "${confskeldirectory}" ]
then
		echo "${myname}: error: Failed to extract confSkelDirectory"
		exit 1
fi
if [ -n "${verbose}" ]
then
	echo "${myname}: info: confSkelDirectory = ${confskeldirectory}"
fi

# verify that replacement counts match
elementcount=${#substitutionstrings[@]}
outputelementcount=${#targetelementnames[@]}
if [ "${elementcount}" -ne "${outputelementcount}" ]
then
		echo "${myname}: error: number of substitutions does not match number of replacements"
		exit 1
fi

# extract substitution values
if [ -n "${verbose}" ]
then
	echo "${myname}: info: Extracting target elements"
fi
index=0
elementcount=${#targetelementnames[@]}
substitutionvalues=()
while [ "${index}" -lt "${elementcount}" ]
do
	targetelementname="${targetelementnames[${index}]}"
	substitutionvalue=`xmlstarlet sel -N tns="${schemauri}" -t -v "${targetelementname}" "${conffile}" | xmlstarlet unesc`
	if [ -z "${substitutionvalue}" ]
	then
			echo "${myname}: error: Failed to extract ${targetelementname}"
			exit 1
	fi
	if [ -n "${verbose}" ]
	then
		echo "${myname}: info: ${targetelementname} = ${substitutionvalue}"
	fi
	escapeStringForSed "${substitutionvalue}"
	escapedsubstitutionvalue="${escapeStringForSedReturn}"
	if [ -n "${verbose}" ]
	then
		echo "${myname}: info: escaped value = ${escapedsubstitutionvalue}"
	fi
	substitutionvalues+=( "${escapedsubstitutionvalue}" )
	((index++))
done

# check that directories exist
if [ -n "${verbose}" ]
then
	echo "${myname}: info: Verifying directories"
fi
if [ ! -d "${confskeldirectory}" ]
then
		echo "${myname}: error: Failed to locate confSkelDirectory"
		exit 1
fi
index=0
elementcount=${#outputfilenames[@]}
while [ "${index}" -lt "${elementcount}" ]
do
	outputdirectory="${outputfilenames[${index}]%/*}"
	if [ ! -d "${outputdirectory}" ]
	then
			echo "${myname}: error: Failed to locate output directory"
			exit 1
	fi
	((index++))
done

# verify input files
index=0
elementcount=${#inputfilenames[@]}
outputelementcount=${#outputfilenames[@]}
if [ "${elementcount}" -ne "${outputelementcount}" ]
then
		echo "${myname}: error: number of input files does not match number of output files"
		exit 1
fi
while [ "${index}" -lt "${elementcount}" ]
do
	# verify that the input file exists
	inputfilename="${inputfilenames[${index}]}"
	inputfilepath="${confskeldirectory}/${servicename}/${inputfilename}"
	outputfilename="${outputfilenames[${index}]}"
	outputdirectory="${outputfilenames[${index}]%/*}"
	if [ -n "${verbose}" ]
	then
		echo "${myname}: info: Verifying skeleton input file"
	fi
	if [ ! -f "${inputfilepath}" ]
	then
		echo "${myname}: error: Failed to locate input file ${inputfilepath}"
		exit 1
	fi
	if [ -z "${outputfilename}" ]
	then
		echo "${myname}: error: Output file name should not be blank for input file : ${inputfilename}"
		exit 1
	fi
	if [ -z "${outputdirectory}" ]
	then
		echo "${myname}: error: Output directory name should not be blank for input file : ${inputfilename}"
		exit 1
	fi
	((index++))
done

# generate a temp directory
if [ -n "${verbose}" ]
then
	echo "${myname}: info: Generating tmp directory"
fi
tmpdir=`mktemp --tmpdir -d "globalconf.${servicename}.XXXXXXXXXX"`
if [ -z "${tmpdir}" ]
then
	echo "${myname}: error: Failed to create temporary directory"
	exit 1
fi
if [ -n "${verbose}" ]
then
	echo "${myname}: info: Tmp directory = ${tmpdir}"
fi
if [ ! -d "${tmpdir}" ]
then
	echo "${myname}: error: Failed to locate temporary directory"
	exit 1
fi

# build the substitution commands
if [ -n "${verbose}" ]
then
	echo "${myname}: info: Building substitution commands"
fi
index=0
elementcount=${#substitutionstrings[@]}
substitutioncommand=""
while [ "${index}" -lt "${elementcount}" ]
do
	substitutionstring="${substitutionstrings[${index}]}"
	substitutionvalue="${substitutionvalues[${index}]}"
	substitutioncommand="${substitutioncommand} -e s/${substitutionstring}/${substitutionvalue}/g"
	((index++))
done

# deal with files
index=0
elementcount=${#inputfilenames[@]}
while [ "${index}" -lt "${elementcount}" ]
do
	inputfilename="${inputfilenames[${index}]}"
	outputfilename="${outputfilenames[${index}]}"

	if [ -n "${verbose}" ]
	then
		echo "${myname}: info: Working on ${inputfilename} -> ${outputfilename}"
	fi

	# output the file
	tmpfilepath="${tmpdir}/${inputfilename}"
	if [ -n "${verbose}" ]
	then
		echo "${myname}: info: Building new configuration file"
	fi
	if ! sed ${substitutioncommand} "${inputfilepath}" > ${tmpfilepath}
	then
		echo "${myname}: error: Failed to output file to temporary directory"
		exit 1
	fi

	# update permissions
	if [ -n "${outputfileperms}" ]
	then
		if [ -n "${verbose}" ]
		then
			echo "${myname}: info: Setting permissions"
		fi
		if ! chmod ${outputfileperms} ${tmpfilepath}
		then
			echo "${myname}: error: Failed to set permissions"
			exit 1
		fi
	fi

	# update ownership
	if [ -n "${outputfileownership}" ]
	then
		if [ -n "${verbose}" ]
		then
			echo "${myname}: info: Setting ownership"
		fi
		if ! chown ${outputfileownership} ${tmpfilepath}
		then
			echo "${myname}: error: Failed to set ownership"
			exit 1
		fi
	fi

	# move the output to the right place
	if [ -n "${verbose}" ]
	then
		echo "${myname}: info: Building new configuration file into place"
	fi
	if ! mv -f "${tmpfilepath}" "${outputfilename}"
	then
		echo "${myname}: error: Failed to move file to output directory"
		exit 1
	fi
	((index++))
done

# restart service
index=0
elementcount=${#restartservices[@]}
while [ "${index}" -lt "${elementcount}" ]
do
	restartservice="${restartservices[${index}]}"
	if [ -n "${restartservice}" ]
	then
		if [ -n "${verbose}" ]
		then
			echo "${myname}: info: Restarting ${restartservice}"
		fi
		if ! invoke-rc.d "${restartservice}" restart
		then
			echo "error: failed to restart ${restartservice}"
			exit 1
		fi
	fi
	((index++))
done

# clean up the tmp dir
if [ -n "${verbose}" ]
then
	echo "${myname}: info: Removing tmp directory"
fi
if ! rm -r "${tmpdir}"
then
	echo "${myname}: error: Failed to remove temporary directory"
	exit 1
fi

